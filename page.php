<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>TITRE DE MA PAGE</title>
        
        <!-- CHARGER ET ASSOCIER LE CODE CSS A CETTE PAGE HTML -->
        <link rel="stylesheet" href="style.css" type="text/css" />
        <style type="text/css">
/* ON PEUT ECRIRE DU CODE CSS DIRECTEMENT DANS LA BALISE HTML style */
        </style>
    </head>
    <body>
        <header>
            <h1>TITRE DE MON SITE</h1>
        </header>
        
        <main>

            <section>
                <h3>MA SECTION HTML</h3>
                <p>DANS CETTE SECTION, IL Y A DU CODE HTML</p>
            </section>

            <section>
                
                
                <h3 class="bouton" onclick="cacherParagraphe();">MA SECTION HTML AVEC DU JAVASCRIPT (cliquer ici)</h3>
                
                <div class="contenu">
                    <p>DANS CETTE SECTION, IL Y A DU CODE HTML ET DU CODE JAVASCRIPT</p>
                </div>
            </section>


            <section>
                <h3>AJOUTER UN ARTICLE</h3>
                <form method="POST" action="#articleCreate" id="articleCreate">
                    <input type="text" name="titre" required placeholder="TITRE"/><br/>
                    <textarea name="contenu" required cols="60" rows="5" placeholder="CONTENU"></textarea><br/>
                    <input type="hidden" name="formTag" value="articleCreate"/>
                    <button type="submit">AJOUTER UN ARTICLE</button>
                    <div class="feedback">
<?php
// TRAITEMENT DU FORMULAIRE

// INFOS DE CONNEXION A LA BASE DE DONNEES MYSQL
// IL FAUT AVEC PHPMYADMIN CONSTRUIRE UNE BASE DE DONNEES demo-webforce
// ET CREER UNE TABLE tableDemo
// AVEC 3 COLONNES
//      id          INT     INDEX=PRIMARY_KEY   AUTO_INCREMENT(A_I)
//      titre       TEXT
//      contenu     TEXT
$databaseMySQL  = "demo-webforce";
$hostMySQL      = "localhost";
$userMySQL      = "root";
$passwordMySQL  = "";

$dsn            = "mysql:host=$hostMySQL;dbname=$databaseMySQL;charset=utf8";


if (!empty($_REQUEST))
{
    // RECUPERER LES INFOS DU FORMULAIRE
    $formTag = $_REQUEST["formTag"];
    if ($formTag == "articleCreate")
    {
        $titre      = $_REQUEST["titre"];
        $contenu    = $_REQUEST["contenu"];
        // SECURITE: VERIFIER LES INFOS
        if ( ($titre != "") && ($contenu != "") )
        {
            $tableauToken = 
            [ ":titre" => $titre, ":contenu" => $contenu ];
            $requeteSQL   = "INSERT INTO tableDemo (titre, contenu) VALUES (:titre, :contenu) ";

            // AJOUTER L'ARTICLE
            $objetPDO           = new PDO($dsn, $userMySQL, $passwordMySQL);
            $objetPDOStatement  = $objetPDO->prepare($requeteSQL);
            $estOK              = $objetPDOStatement->execute($tableauToken);

            echo "ARTICLE AJOUTE";
        } 
    }
}
?>                        
                    </div>
                </form>
            </section>
            
            <section>
                <h3>LISTE DES ARTICLES</h3>
                <pre>
LE CONTENU HTML DE CETTE SECTION EST CREE PAR PHP DYNAMIQUEMENT
(AVEC DES INFOS STOCKEES DANS MYSQL)
                </pre>
               
                <?php
// ON ECRIT DU CODE PHP


$requeteSQL     = "SELECT * from tableDemo ORDER BY id DESC";

$objetPDO           = new PDO($dsn, $userMySQL, $passwordMySQL);
$objetPDOStatement  = $objetPDO->query($requeteSQL);

// LECTURE DES LIGNES TROUVEES
while($tabLigne = $objetPDOStatement->fetch() )
{
    // RECUPERER LES INFOS DES COLONNES POUR CHAQUE LIGNE
    $titre      = $tabLigne["titre"];
    $contenu    = $tabLigne["contenu"];
    
    // CONSTRUCTION ET AFFICHAGE DU CODE HTML
    echo 
<<<CODEHTML

    <article>
        <h3>$titre</h3>
        <p>$contenu</p>
    </article>
    
CODEHTML;
}
                ?>
            </section>
        </main>
        
        <footer>
            <div>tous droits réservés</div>
        </footer>
        
        <!-- CHARGER ET ASSOCIER LE CODE JAVASCRIPT A CETTE PAGE HTML -->
        <script type="text/javascript" src="script.js">
        </script>
        
        <script type="text/javascript">
/* ON PEUT ECRIRE DU CODE JAVASCRIPT DANS LA BALISE script */        
        </script>


    </body>
</html>