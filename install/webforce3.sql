-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 26 Décembre 2016 à 16:53
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `demo-webforce`
--
CREATE DATABASE IF NOT EXISTS `demo-webforce` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `demo-webforce`;

-- --------------------------------------------------------

--
-- Structure de la table `tabledemo`
--

DROP TABLE IF EXISTS `tabledemo`;
CREATE TABLE IF NOT EXISTS `tabledemo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `contenu` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vider la table avant d'insérer `tabledemo`
--

TRUNCATE TABLE `tabledemo`;
--
-- Contenu de la table `tabledemo`
--

INSERT INTO `tabledemo` (`id`, `titre`, `contenu`) VALUES
(1, 'titre1', 'contenu1'),
(2, 'titre2', 'contenu2'),
(3, 'titre3', 'contenu3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
